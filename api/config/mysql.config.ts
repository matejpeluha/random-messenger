import {Connection, createConnection} from "mysql";
import {ConnectionOptions} from "../models/interfaces/options/ConnectionOptions";


const connectionOptions: ConnectionOptions = {
    host: 'eu-cdbr-west-01.cleardb.com',
    user: 'bb491063e8a4e6',
    password: '229d707c',
    database: 'heroku_2bc74d88e690c76'
};

const connection: Connection = createConnection(connectionOptions);

export {connection};