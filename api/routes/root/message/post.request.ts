import {Request, Response} from "express";
import {MessageSaverService} from "../../../services/MessageSaver.service";

const postRequest = (req: Request, res: Response): void =>{
    const messageSaver: MessageSaverService = new MessageSaverService(req, res);
    messageSaver.saveMessage();
}

export {postRequest};