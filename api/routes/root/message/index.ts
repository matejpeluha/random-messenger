import {Router} from "express";
import {postRequest} from "./post.request";
import {getRequest} from "./get.request";

const message: Router = Router();

message.get("/", getRequest);
message.post("/", postRequest);

export {message};