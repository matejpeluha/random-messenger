import {Request, Response} from "express";
import {MessageRandomizerService} from "../../../services/MessageRandomizer.service";


const getRequest = (req: Request, res: Response): void =>{
    const messageRandomizer: MessageRandomizerService = new MessageRandomizerService(req, res);
    messageRandomizer.getRandomMessage();
}

export {getRequest};