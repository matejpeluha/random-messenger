import {Request, Response} from "express";

const getRequest = (req: Request, res: Response): void =>{
    res.json({answer: "Hello"});
}

export {getRequest};