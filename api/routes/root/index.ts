import {Router} from "express";
import {getRequest} from "./get.request";
import {message} from "./message";

const root: Router = Router();

root.use("/message", message);

root.get("/", getRequest);

export {root};