import {ApiMysqlRequest} from "./abstract/ApiMysqlRequest";
import {Request, Response} from "express";
import {MysqlError} from "mysql";
import {RandomMessageJson} from "../models/interfaces/jsons/RandomMessageJson";

class MessageRandomizerService extends ApiMysqlRequest{

    constructor(req: Request, res: Response) {
        super(res);

    }

    public getRandomMessage(): void{
        const queryString: string = "SELECT text as messageText, author, timestamp as timeStamp FROM messages JOIN message_info m_i on m_i.id = messages.info_id ORDER BY RAND() LIMIT 1"
        const values: (number | string)[] = [];

        this.connection.query(queryString, values, this.handleGetRandomMessage);
    }

    private handleGetRandomMessage = (err: MysqlError | null, results: RandomMessageJson[]): void => {
        if(err){
            this.res.status(500).send(err.sqlMessage);
        }
        else if(results.length < 1){
            this.res.status(404).send("Messages does not exist.");
        }
        else{
            const jsonObject: RandomMessageJson = results[0];
            this.res.json(jsonObject);
        }
    }
}

export {MessageRandomizerService};