import {Connection} from "mysql";
import {Response} from "express";
import {connection} from "../../config/mysql.config";

abstract class ApiMysqlRequest{
    protected connection: Connection;
    protected res: Response;

    protected constructor(res: Response) {
        this.connection = connection;
        this.res = res;
    }
}

export {ApiMysqlRequest};
