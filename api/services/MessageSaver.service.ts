import {ApiMysqlRequest} from "./abstract/ApiMysqlRequest";
import {Request, Response} from "express";
import {MysqlError} from "mysql";
import {ChangeResultMysqlJson} from "../models/interfaces/jsons/ChangeResultMysqlJson";

class MessageSaverService extends ApiMysqlRequest{
    private readonly messageText: string;
    private readonly author: string;
    private readonly timeStamp: number;

    constructor(req: Request, res: Response) {
        super(res);
        this.messageText = req.body.messageText;
        this.author = req.body.author;
        this.timeStamp = req.body.timeStamp;

    }

    public saveMessage():void {
        const queryString: string = "INSERT INTO message_info(author, timestamp) VALUES (?, ?)";
        const values: (string | number)[] = [this.author, this.timeStamp];

        this.connection.query(queryString, values, this.handleSaveMessage);
    }

    private handleSaveMessage = (err: MysqlError | null, result: ChangeResultMysqlJson): void => {
        if(err){
            this.res.status(500).send(err.sqlMessage);
        }
        else{
            const infoId: number = result.insertId;
            this.saveMessageText(infoId);
        }
    }

    private saveMessageText(infoId: number): void{
        const queryString: string = "INSERT INTO messages (text, info_id) VALUES (?, ?)";
        const values: (string | number)[] = [this.messageText, infoId];

        this.connection.query(queryString, values, this.handleSaveMessageText);
    }

    private handleSaveMessageText = (err: MysqlError | null, result: ChangeResultMysqlJson): void => {
        if(err){
            this.res.status(500).send(err.sqlMessage);
        }
        else{
            this.res.sendStatus(200);
        }

        this.res.end();
    }

}

export {MessageSaverService};