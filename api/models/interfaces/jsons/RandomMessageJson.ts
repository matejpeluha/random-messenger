interface RandomMessageJson{
    messageText: string,
    author: string,
    timeStamp: number
}

export {RandomMessageJson};