import {Router} from "express";
import {root} from "./routes/root";
import path from "path";


const apiRouter: Router = Router();

apiRouter.use("/api", root);


// A CATCH ALL ROUTE TO SERVE OUR INDEX FILE IF THE PATH IS NOT MATCHED AGAINST EXISTING ROUTES
apiRouter.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '..', 'Client', 'build', 'index.html'));
});


export {apiRouter};