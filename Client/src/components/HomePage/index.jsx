import React, {Component, Fragment} from "react";

import "./index.css";
import Footer from "../Footer";

class HomePage extends Component {
    render() {
        return (
            <Fragment>
                <main className="background">
                    <div className="content">
                        <h1>
                            Random Messenger
                        </h1>
                        <div className="button-container">
                            <a className="button" href="/send">
                                send.
                            </a>
                            <a className="button" href="/receive">
                                receive.
                            </a>
                        </div>
                    </div>
                </main>
                <Footer/>
            </Fragment>
        );
    }
}

export default HomePage;