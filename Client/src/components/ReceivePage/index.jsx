import React, {Component} from "react";

import "./index.css";
import NavBar from "../NavBar";

class ReceivePage extends Component {
    state = {
        messageText: "",
        author: "",
        timeStamp: 0
    }

    render() {
        return (
            <main className="background">
                <NavBar secondLink="/send" secondLinkName="Send"/>
                <section className="receive-container">
                    <h3>
                        Receive random message
                    </h3>
                    <div className="message-container">
                        <p className="message">
                            {this.showMessage()}
                            <br/><br/>
                            <em>{this.getInfo()}</em>
                        </p>
                    </div>
                    <button className="receive-button" onClick={this.handleOnClickButton}>
                        Show
                    </button>
                </section>
            </main>
        );
    }

    handleOnClickButton = () =>{
        let reqHeader = new Headers();
        reqHeader.append('Content-Type', 'application/json');

        let initObject = {
            method: 'GET', headers: reqHeader,
        };

        const userRequest = new Request('/api/message', initObject);

        fetch(userRequest)
            .then(this.handleJson)
            .then(this.saveMessage)
            .catch(function (err) {
                console.log("Something went wrong!", err);
            });
    }

    handleJson = (response) =>{
        return response.json();
    }

    saveMessage = (data) =>{
        this.setState(data);
    }

    showMessage(){
        if(this.state.author === "" && this.state.timeStamp === 0){
            return "";
        }
        else{
            return "'" + this.state.messageText + "'";
        }
    }

    getInfo(){
        if(this.state.author === "" && this.state.timeStamp === 0){
            return "";
        }
        else{
            return "-" + this.state.author + ", " + this.getDate()
        }
    }

    getDate(){
        const date = new Date(this.state.timeStamp * 1000);
        console.log(date)
        const day = date.getDate();
        const month = date.getMonth() + 1;
        const year = date.getFullYear();
        return day + "." + month  + "." + year;
    }
}

export default ReceivePage;