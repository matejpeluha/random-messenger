import React, {Component} from "react";

import "./index.css";

class NotFoundPage extends Component {
    render() {
        return (
            <div>
                <h1>
                    404 - PAGE NOT FOUND
                </h1>
            </div>
        );
    }
}

export default NotFoundPage;