import React, {Component} from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faThumbsUp, faThumbsDown} from "@fortawesome/free-solid-svg-icons";

import "./index.css";

class SendModal extends Component {
    render() {
        return (
            <main id="modal-background">
                <section id="modal-content">
                    {this.getContent()}
                </section>
            </main>
        );
    }

    getContent(){
        if(this.props.success){
            return this.getSuccess();
        }
        else{
            return this.getFailure();
        }
    }

    getSuccess(){
        return (
            <div className="answer-container">
                <div className="answer-centralizer">
                    <h1 className="success-header">Message sent</h1>
                    <h5>Please keep politeness in messages</h5>
                    <FontAwesomeIcon icon={faThumbsUp} className="success-icon" />
                </div>
                <div className="button-container">
                    <button className="modal-button" onClick={this.props.onCloseModal}>
                        Send more!
                    </button>
                </div>
            </div>
        )
    }

    getFailure(){
        return (
            <div className="answer-container">
                <div className="answer-centralizer">
                    <h1 className="failure-header">Sorry, something went wrong...</h1>
                    <FontAwesomeIcon icon={faThumbsDown} className="failure-icon" />
                </div>
                <div className="button-container">
                    <button className="modal-button" onClick={this.props.onCloseModal}>
                        Try again
                    </button>
                </div>
            </div>
        );
    }
}

export default SendModal;