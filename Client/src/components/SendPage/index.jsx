import React, {Component} from "react";

import "./index.css";
import NavBar from "../NavBar";
import SendModal from "../SendModal";

class SendPage extends Component {
    state = {
        showModal: false,
        success: true
    }
    render() {
        return (
            <main className="background">
                <NavBar secondLink="/receive" secondLinkName="Receive"/>
                <section className="send-container">
                    <h3>
                        Send your message
                    </h3>
                    <form className="form-container" onSubmit={this.handleSubmitForm}>
                        <input type="text" placeholder="Name" id="name-input" required={true}/>
                        <textarea placeholder="Your message" id="text-area" maxLength="4096" required={true}/>
                        <button type="submit" className="send-button">
                            Send
                        </button>
                    </form>
                </section>
                {this.state.showModal ? <SendModal success={this.state.success} onCloseModal={this.handleCloseModal}/> : null}
            </main>
        );
    }

    handleSubmitForm = (e) =>{
        e.preventDefault();

        const json = this.getPostJson();

        let reqHeader = new Headers();
        reqHeader.append('Content-Type', 'application/json');

        let initObject = {
            method: 'POST', headers: reqHeader, body: json
        };

        const userRequest = new Request('/api/message', initObject);

        fetch(userRequest)
            .then(this.handleResponse)
            .catch(function (err) {
                console.log("Something went wrong!", err);
            });
        return false;
    }

    getPostJson(){
        const json = {
            author: document.getElementById("name-input").value,
            messageText: document.getElementById("text-area").value,
            timeStamp: (new Date()).getTime() / 1000
        };
        return  JSON.stringify(json);
    }

    handleResponse = (response) =>{
        if(response.status === 200){
            this.setState({showModal: true, success: true});
        }
        else{
            this.setState({showModal: true, success: false});
        }
    }


    handleCloseModal = () =>{
        if(this.state.success){
            document.getElementById("name-input").value = "";
            document.getElementById("text-area").value = "";
        }
        this.setState({showModal: false, success: this.state.success});
    }



}

export default SendPage;