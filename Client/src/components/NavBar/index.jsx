import React, {Component} from "react";

import "./index.css";

class NavBar extends Component {
    render() {
        return (
            <nav className="navbar">
                <a href="/">Home</a>
                <a href={this.props.secondLink}>{this.props.secondLinkName}</a>
            </nav>
        );
    }
}

export default NavBar;