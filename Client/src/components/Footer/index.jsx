import React, {Component} from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faNodeJs, faReact } from '@fortawesome/free-brands-svg-icons'

import "./index.css";

class Footer extends Component {
    render() {
        return (
            <footer className="footer-body">
                Powered by React <FontAwesomeIcon className="footer-icon" icon={faReact} /> and Node <FontAwesomeIcon className="footer-icon" icon={faNodeJs} />
            </footer>
        );
    }
}

export default Footer;