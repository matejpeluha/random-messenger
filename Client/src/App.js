
import './App.css';
import {Route, BrowserRouter as Router, Switch} from "react-router-dom";
import HomePage from "./components/HomePage";
import ReceivePage from "./components/ReceivePage";
import SendPage from "./components/SendPage";
import NotFoundPage from "./components/NotFoundPage";

function App() {
  return (
      <Router basename={"/"}>
          <Switch>
              <Route path={"/"} exact component={HomePage}/>
              <Route path={"/receive"} exact component={ReceivePage}/>
              <Route path={"/send"} exact component={SendPage}/>
              <Route component={NotFoundPage} />
          </Switch>
      </Router>

  );
}

export default App;
